package INF101.lab2;
import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    
    int max_size = 20;          //Sets the maximum size of the fridge to 20 units/items
    List<FridgeItem> fridge;
    List<FridgeItem> expiredItems;

    public Fridge(){
        fridge = new ArrayList<FridgeItem>();           //creats the fridge array
        expiredItems = new ArrayList<FridgeItem>();     // creats the expiredItems array
    }

    public int totalSize(){         //"defines" a method for size of fridge used by IFridge
        return max_size;
    }
    
    public int nItemsInFridge(){        //"defines" a method for determening items in fridge used by IFridge
        return fridge.size();
    }

    
	public boolean placeIn(FridgeItem item){        //"defines" a method for checking if there is room in the fridge used by IFridge
        if (fridge.size()<max_size){
            fridge.add(item);
            return true;
        }
        return false;
    }


    public void takeOut(FridgeItem item){         //"defines" a method for removing items from fridge used by IFridge
        if (fridge.contains(item)){
            fridge.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
    }

	public void emptyFridge(){              //"defines" a method for removing all items from fridge used by IFridge
        fridge.clear();
    }


	public List<FridgeItem> removeExpiredFood(){        //"defines" a method for removing expired items from fridge used by IFridge
        for (FridgeItem f : fridge){        // Loops through items in "fridge"
            if(f.hasExpired()){             //Checks if item is expired (code from "fridgeItem")
                expiredItems.add(f);        // adds expired items to a list
            }
        }
        for (FridgeItem f : expiredItems){      // removes expired items from fridge
            fridge.remove(f);
        }
        return expiredItems;
    }

}
